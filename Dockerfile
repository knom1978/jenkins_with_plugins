#this is the base image we use to create our image from
FROM jenkins/jenkins:2.199-jdk11

#just info about who created this
MAINTAINER Cecil Liu (cecil_liu@umc.com)

#get rid of admin password setup
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"
COPY security.groovy /usr/share/jenkins/ref/init.groovy.d/security.groovy
COPY cofigure-jenkins-url.groovy /usr/share/jenkins/ref/init.groovy.d/cofigure-jenkins-url.groovy
#automatically installing all plugins
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
