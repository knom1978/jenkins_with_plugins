Jenkins docker-compose solution with pre-installed plugins
===
###### tags: `Jenkins` `docker-compose`
## Introduction
A docker-compose solution of Jenkins

## How to use
a. To git clone this folder, and cd kenkins_with_plugins

b. To create data folder
```
mkdir ./data
sudo chown -R 1000 ./data
```
c. To creat two files, which are named as JENKINS-USER and JENKINS-PASS.

d. To fill username and password into two files (JENKINS-USER amd JENKINS-PASS)

e. Then use docker-compose.
```
docker-compose up -d
```

## Reference
* The method to install plugins through extracting from Jenkins

https://dzone.com/articles/dockerizing-jenkins-2-setup-and-using-it-along-wit

* The method to automated Jenkins by docker-compose

https://technologyconversations.com/2017/06/16/automating-jenkins-docker-setup/

* How to solve the issue : dial unix /var/run/docker.sock: connect: permission denied

https://blog.csdn.net/luckytonyzhang/article/details/89059415
```
sudo chmod 666 /var/run/docker.sock
```

## The next plan
* Master/slave test

https://www.howtoforge.com/tutorial/ubuntu-jenkins-master-slave/
https://www.cnblogs.com/stulzq/p/9297260.html

* Jenkinsfile try-run

https://www.cnblogs.com/stulzq/p/10115589.html

